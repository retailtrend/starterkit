export interface IEarlgreyTaskConfig {
  /// Root directory of frontend source
  frontendPath?: string
  /// Root directory of backend source
  backendPath?: string
  /// Root directory of build result
  buildPath?: string
  /// Root directory of test source
  testPath?: string
  testBuildPath?: string

  /// Build output directory of backend. This path is used to restart server when change files
  backendBuildPath?: string
  /// Build output directory of frontend. This path is used for reload browser when change files
  frontendBuildPath?: string
  /// number of port of BS
  port?: number
  /// Output destionation of apidoc
  apidocPath: string
  /// Options for generate my.ts
  envConfig?: {
    /// path of env config (eg: src/config/environment)
    path: string
    my?: {
      /// Template of my.ts
      template: string
      /// Template variables
      variables: object
    }
  }
  webpack?: {
    entry: string[]
    polyfills: string[]
    babel: object
    mangle: boolean
    htmlPlugins: object[]
    publicPath: string
  }
  sequalizeAuto?: object
  apidocS3?: {
    region: string
    bucket: string
    prefix: string
  }
  cdnS3?: {
    region: string
    bucket: string
    prefix: string
  }
}

export interface IEarlgreyTaskOption {
  buildTasks?: any[]
  serveTasks?: any[]
}

export declare function earlGreyTasks(packageJson: object, config?: IEarlgreyTaskConfig, options?: IEarlgreyTaskOption): any
