type Config = {
  path: string
  my?: {
    /// custom mustache template string
    template?: string
    variables?: any
  }
}

export declare function generateMy(cfg: Config)
