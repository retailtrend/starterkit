"use strict"

var sourcemaps = require("gulp-sourcemaps")
var sass = require("gulp-sass")
var gls = require("gulp-live-server")
var watch = require("gulp-watch")
var babel = require("gulp-babel")
var Cache = require("gulp-file-cache")
var path = require("path")
var sequelizeAuto = require("sequelize-auto")
var _ = require("lodash")

var globlify = require("../lib/globlify.js")
var webpackExec = require("../bin/webpack")

if (!process.env.NODE_ENV) process.env.NODE_ENV = "development"

//wsl: inotify error - https://github.com/Microsoft/BashOnWindows/issues/216

const createWebpackGls = (configPath, webpackConfig, port) => {
  // Create temporary config
  var webpackOptions = [`--config=${configPath}`]
  if (process.env.NODE_ENV == "development") webpackOptions = webpackOptions.concat(["--hot"])
  else webpackOptions = webpackOptions.concat(["--watch"])

  return gls(
    [path.join(__dirname, "../bin/webpack")].concat(webpackOptions).concat(webpackConfig.assetsPath),
    {
      env: process.env,
    },
    port
  )
}

module.exports = function (
  gulp,
  application,
  starterkitConfigPath = path.join(__dirname, "../config/starterkit-config.example.js"),
  options = {}
) {
  options.gls = options.gls || { minPort: 35730 }

  var starterkitConfig = require(starterkitConfigPath)
  var webpackConfig = starterkitConfig.webpack
  var babelConfig = starterkitConfig.babel

  var sassPath = globlify(webpackConfig.assetsPath, ["sass", "scss"])
  var sassOutput = path.join(webpackConfig.publicPath, "assets", "styles")

  var buildPath = [path.normalize(babelConfig.buildPath), `!${path.join(babelConfig.buildPath, "webpack-assets.json")}`]
  var serverPath = {
    src: globlify(babelConfig.srcPath, ["js"]).concat("!./**/node_modules/**"),
    dest: babelConfig.buildPath,
  }
  gulp.task("sass", (f) => {
    let sassOption = {
      outputStyle: "uncompressed",
      includePaths: webpackConfig.assetsPath.concat(webpackConfig.sass.resolvePath),
    }
    return gulp.src(sassPath).pipe(sass(sassOption).on("error", sass.logError)).pipe(gulp.dest(sassOutput))
  })

  gulp.task("webpack", function (cb) {
    return webpackExec({ config: starterkitConfigPath })
  })

  let cache = new Cache()
  let babelDo = () => {
    return gulp
      .src(serverPath.src)
      .pipe(sourcemaps.init())
      .pipe(cache.filter())
      .pipe(
        babel({
          presets: ["es2017", "stage-0", "node6"],
        })
      )
      .on("error", function (err) {
        console.error(err)
        this.emit("end")
      })
      .pipe(cache.cache())
      .pipe(sourcemaps.write("./", { sourceRoot: "../server" }))
      .pipe(gulp.dest(serverPath.dest))
  }

  gulp.task("babelInternal", () => {
    return babelDo()
  })

  let babelDepends = []
  if (!_.isEmpty(options.sequelizeAuto)) babelDepends.push("sequelizeAuto")
  gulp.task("babel", babelDepends, () => {
    return babelDo()
  })

  const webpackServer = createWebpackGls(starterkitConfigPath, webpackConfig, options.gls.minPort++)
  var appServer = gls(
    [path.join(babelConfig.buildPath, "start.js")],
    {
      env: process.env,
    },
    options.gls.minPort++
  )

  gulp.task("webpack-watch", () => {
    webpackServer.start()
  })
  gulp.task("babel-watch", () => {
    return watch(serverPath.src, { usePolling: true }, () => {
      gulp.start("babelInternal")
    })
  })

  gulp.task("server-watch", () => {
    appServer.start()
    return watch(buildPath, { usePolling: true }, (f) => {
      appServer.start.bind(appServer)()
    })
  })

  gulp.task("sass-watch", () => {
    return watch(sassPath, { usePolling: true }, () => {
      gulp.start("sass")
    })
  })

  if (!_.isEmpty(options.sequelizeAuto)) {
    gulp.task("sequelizeAuto", () => {
      let autoConfig = options.sequelizeAuto || {
        spaces: false,
        indentation: 1,
        directory: "schema",
      }
      let seqOptions = options.sequelize
      autoConfig.host = seqOptions.host
      var auto = new sequelizeAuto(seqOptions.database, seqOptions.username, seqOptions.password, autoConfig)
      var defer = new Promise((resolve, reject) => {
        auto.run(function (err) {
          if (err) {
            console.error(err)
            reject(err)
          } else {
            resolve()
          }
        })
      })

      return defer
    })
  }

  gulp.task("lint", (done) => {
    // Should override outer
    done()
  })

  gulp.task("test", (done) => {
    // Should override outer
    done()
  })

  gulp.task("build", ["babelInternal", "sass", "webpack"])
  gulp.task("serve", ["babel", "sass"], () => {
    return Promise.all([gulp.start("webpack-watch"), gulp.start("babel-watch"), gulp.start("sass-watch"), gulp.start("server-watch")])
  })
  gulp.task("run", ["serve"])
  gulp.task("default", ["run"])
}
