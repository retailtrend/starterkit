const path = require("path")
const webpack = require("webpack")
const CompressionPlugin = require("compression-webpack-plugin")
const TerserPlugin = require("terser-webpack-plugin")
const HtmlPlugin = require("html-webpack-plugin")
const HtmlWebpackHarddiskPlugin = require("html-webpack-harddisk-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin")
const _ = require("lodash")

module.exports = (config) => {
  if (!config.babel) {
    config.babel = {}
  }
  if (!config.sassOptions) config.sassOptions = {}
  if (!config.sassOptions.loadPaths) config.sassOptions.loadPaths = []
  const devMode = process.env.NODE_ENV !== "production"

  let filenameTemplate = "[name].[contenthash]"
  let chunkFilenameTemplate = "[id].[contenthash]"
  if (devMode) {
    filenameTemplate = "[name]"
    chunkFilenameTemplate = "[id]"
  }
  var plugins = [
    new webpack.DefinePlugin({
      "process.env": {
        STAGE_ENV: JSON.stringify(process.env.STAGE_ENV),
        CONFIG_PATH: JSON.stringify(process.env.CONFIG_PATH), // TODO: Remove It!
      },
    }),
    new webpack.ids.HashedModuleIdsPlugin(),
    new CopyWebpackPlugin({ patterns: [{ from: "assets", to: "assets" }] }),
    new MiniCssExtractPlugin({
      filename: `${filenameTemplate}.css`,
      chunkFilename: `${chunkFilenameTemplate}.css`,
    }),
  ]

  if (config.htmlPlugins) {
    let minify
    if (devMode) minify = false
    else
      minify = {
        removeAttributeQuotes: true,
        collapseWhitespace: true,
        html5: true,
        minifyCSS: true,
        removeComments: true,
        removeEmptyAttributes: true,
      }
    plugins = plugins.concat(
      config.htmlPlugins.map(
        (opts) =>
          new HtmlPlugin({
            ...opts,
            minify: minify,
            alwaysWriteToDisk: true,
          })
      )
    )
    if (config.htmlPlugins.length > 0)
      plugins = plugins.concat(
        new HtmlWebpackHarddiskPlugin({
          outputPath: config.distPath,
        })
      )
  }
  var optimization = {
    splitChunks: {
      chunks: "all",
    },
    minimizer: [
      new TerserPlugin({
        extractComments: true,
        parallel: true,
        terserOptions: {
          mangle: config.mangle,
        },
      }),
      new CssMinimizerPlugin(),
    ],
  }

  if (!devMode) {
    plugins.unshift(new CompressionPlugin())
  }

  let devTools = "source-map"
  let entries = {}
  if (config.entry && config.entry.length > 0) {
    config.entry.forEach((e) => {
      entries[e] = e
    })
  }

  const loaders = {
    coffee: {
      test: /\.coffee$/,
      use: [
        {
          loader: "babel-loader",
          options: {
            envName: "webpack",
            cacheDirectory: true,
          },
        },
        {
          loader: "coffee-loader",
          options: {
            sourceMap: true,
          },
        },
      ],
      exclude: /node_modules\//,
    },
    babel: {
      test: /\.[jt]sx?$/,
      use: [
        {
          loader: "babel-loader",
          options: {
            envName: "webpack",
            cacheDirectory: true,
          },
        },
      ],
      exclude: /(node_modules|\.d\.ts$)/,
      ...config.babel,
    },
    html: {
      test: /\.html$/,
      use: [
        {
          loader: "html-loader",
          options: {
            minimize: !devMode,
          },
        },
      ],
    },
    file: {
      test: /\.(eot|woff2?|ttf|svg|png|jpg|gif|ico)$/,
      type: "asset/resource",
    },
    css: {
      test: /\.(s[ac]|c)ss$/i,
      use: [
        MiniCssExtractPlugin.loader,
        "css-loader",
        {
          loader: "resolve-url-loader",
          options: {
            sourceMap: !devMode,
          },
        },
        {
          loader: "sass-loader",
          options: {
            sourceMap: true,
            implementation: require("sass"),
          },
        },
      ],
    },
  }
  const curPath = path.join(process.cwd(), "node_modules")

  const webpackConfig = {
    mode: process.env.NODE_ENV || "development",
    entry: entries,
    output: {
      path: config.distPath,
      publicPath: config.publicPath,
      filename: `${filenameTemplate}.js`,
    },
    context: config.srcPath,
    plugins: plugins,
    module: {
      rules: Object.values(loaders),
    },
    devtool: devTools,
    optimization,
    resolve: _.merge(
      {
        extensions: [".ts", ".tsx", ".js", ".jsx", ".coffee", ".css", ".scss", ".sass"],
        //modules: [path.resolve(`${__dirname}/../../../../node_modules`), path.resolve(`${__dirname}/../../node_modules`)],
      },
      config.resolve
    ),
  }

  // pnpm hack
  // uiot/node_modules/.bitbucket.org/retailtrend/starterkit/ed2c77/node_modules/starterkit/dist/lib
  if (process.env.PNPM == 1) {
    webpackConfig.resolveLoader = {
      modules: [__dirname + "/../../..", curPath],
    }
  }

  return webpackConfig
}
