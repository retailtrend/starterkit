# QM Application common deploy tasks.
# require capistrano property ':application'

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'node_modules')
set :linked_files, fetch(:linked_files, []).push('config/log.json')
set :default_env, 'NODE_ENV' => 'production'
set :nvm, '$HOME/.nvm/nvm-exec'

namespace :deploy do
  sequelize = '../../node_modules/.bin/sequelize' # relative for server/persistence
  gulp = '../node_modules/.bin/gulp'              # relative for server

  def nvm_execute(*args)
    args.unshift(fetch(:nvm))
    execute(*args)
  end

  task :build do
    on roles(:app) do
      within("#{release_path}/server") do
        nvm_execute gulp, 'babel', 'webpack'
      end
    end
  end

  namespace :db do
    task :migrate do
      on roles(:app) do
        within "#{release_path}/server/persistence" do
          nvm_execute sequelize, 'db:migrate'
        end
      end
    end

    task :rollback do
      on roles(:app) do
        within "#{release_path}/server/persistence" do
          nvm_execute sequelize, 'db:migrate:undo'
        end
      end
    end
  end

  task :nvm_install do
    on roles(:app) do
      within release_path do
        version = capture(:cat, '.nvmrc')
        execute "source $HOME/.nvm/nvm.sh && nvm install #{version} 2> /dev/null"
      end
    end
  end

  task :yarn do
    on roles(:app) do
      within release_path do
        nvm_execute 'yarn'
      end
    end
  end

  task :stop do
    on roles(:app) do
      within release_path do
        execute :screen, '-X', '-S', "gulp_#{fetch(:application)}", :quit, raise_on_non_zero_exit: false
      end
    end
  end

  task :start do
    on roles(:app) do
      within release_path do
        execute :screen, '-dmS', "gulp_#{fetch(:application)}", fetch(:nvm), :npm, 'run-script', :gulp
      end
    end
  end

  task :restart do
    invoke 'deploy:stop'
    invoke 'deploy:start'
  end
end
