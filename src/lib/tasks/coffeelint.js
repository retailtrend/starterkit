const gulpCoffeelint = require("gulp-coffeelint")
const vfs = require("vinyl-fs")
const cache = require("gulp-cached")
const map = require("map-stream")

module.exports = ({ srcPattern }) => {
  const coffeelint = (done) => {
    let errorCount = 0
    return vfs
      .src(srcPattern)
      .pipe(cache("coffeelint"))
      .pipe(gulpCoffeelint())
      .pipe(
        map((data, cb) => {
          const coffeelint = data.coffeelint
          if (coffeelint) errorCount += coffeelint.errorCount
          cb(null, data)
        })
      )
      .pipe(gulpCoffeelint.reporter())
      .on("end", () => {
        if (done) {
          if (errorCount > 0) return done(new Error("Detect erorrs on lint"))
          done()
        }
      })
  }

  return coffeelint
}
