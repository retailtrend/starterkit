const _nodemon = require("nodemon")
const waitPort = require("wait-port")
const _ = require("lodash")

module.exports = ({ watchPath, exec, options = {}, callbacks = {} }) => {
  const nodemon = (done) => {
    try {
      const nodemonOpts = {
        script: exec,
        execMap: {
          js: "node --enable-source-maps --trace-deprecation --trace-warnings --unhandled-rejections=strict",
        },
        watch: watchPath,
      }
      if (options.watchIgnorePatterns) {
        nodemonOpts.ignore = options.watchIgnorePatterns
      }
      _nodemon(nodemonOpts)
        .on("crash", () => {
          console.warn("server was closed with errors")
        })
        .on("start", () => {
          console.info("server has been started")
        })
        .on("restart", (files) => {
          const msg = "server restarting cause"
          if (!files) {
            console.info(`${msg} 'rs'`)
          } else {
            console.info(`${msg} ${files.join(",")}`)
          }
        })
        .on("exit", () => {
          console.info(`server was closed`)
        })
        .on("quit", () => {
          console.info("nodemon quit")
          _nodemon.emit("exit")
          done()
          process.kill(process.pid, "SIGINT")
        })

      if (_.isFunction(callbacks.start)) {
        if (options.appPort) {
          waitPort({
            host: "localhost",
            port: options.appPort,
          }).then((opened) => {
            if (opened && callbacks.start) callbacks.start()
            else console.warn("Application not started before timeout")
          })
          console.info("")
        } else {
          callbacks.start()
        }
      }
    } catch (err) {
      console.error(err)
    }
  }

  return nodemon
}
