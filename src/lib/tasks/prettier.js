const prettier = require("prettier")
const map = require("map-stream")
const cosmiconfig = require("cosmiconfig")
const vfs = require("vinyl-fs")

module.exports =
  ({ srcPattern }) =>
  (done) => {
    return cosmiconfig("prettier", { rcExtensions: true })
      .load()
      .then((result) => {
        return vfs
          .src(srcPattern)
          .pipe(
            map((data, cb) => {
              if (!prettier.check(data.contents.toString(), result.config)) {
                const err = new Error("Failed to check prettier")
                err.file = data
                return cb(err)
              }
              cb(null, data)
            })
          )
          .on("error", (err) => {
            console.error(`${err.file.path}: ${err.message}`)
            done(err)
          })
      })
      .catch((err) => {
        done(err)
      })
  }
