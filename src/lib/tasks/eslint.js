const map = require("map-stream")
const { ESLint } = require("eslint")
const vfs = require("vinyl-fs")
const cache = require("gulp-cached")

module.exports = ({ srcPattern }) => {
  const eslint = (done) => {
    const eslint = new ESLint()
    const formatter = eslint.loadFormatter("unix")
    let errorCount = 0
    return vfs
      .src(srcPattern)
      .pipe(cache("eslint"))
      .pipe(
        map(async (data, cb) => {
          try {
            if (await eslint.isPathIgnored(data.path)) {
              return cb(null, [])
            }
            const result = await eslint.lintFiles(data.path)
            cb(null, result)
          } catch (err) {
            cb(err, null)
          }
        })
      )
      .pipe(
        map((data, cb) => {
          formatter.then((formatter) => {
            const messages = formatter.format(data)
            if (messages) {
              errorCount += data.reduce((prev, cur) => prev + cur.errorCount, 0)
              console.warn(messages)
            }
            cb(null, formatter.format(data))
          })
        })
      )
      .on("error", (err) => done(err))
      .on("end", () => {
        if (done) {
          if (errorCount > 0) {
            done(new Error("ESLint"))
          } else {
            done()
          }
        }
      })
  }

  eslint.displayName = "eslint"

  return eslint
}
