import { spawn } from "child_process"
import _ from "lodash"

const mochaTask = ({ srcPattern, options = {} }) => {
  const task = (done) => {
    const newDone = (err) => {
      if (done) done(err)
    }
    if (!_.isArray(srcPattern)) srcPattern = [srcPattern]
    const args = ["--exit"].concat(srcPattern)
    const child = spawn("node_modules/.bin/mocha", args, {
      stdio: "inherit",
    })
    child.on("exit", (code, signal) => {
      if (code != 0) newDone(new Error("exit with error"))
      else newDone()
    })

    // const mocha = new Mocha(options)
    // return vfs
    //   .src(srcPattern)
    //   .pipe(
    //     map((data, cb) => {
    //       try {
    //         delete require.cache[require.resolve(data.path)]
    //       } catch (err) {
    //         console.log(err)
    //       }
    //       mocha.addFile(data.path)
    //       cb(null, data.path)
    //     })
    //   )
    //   .on("end", () => {
    //     mocha.run(err => {
    //       if (done) {
    //         if (options.ignoreFailure) done()
    //         else done(err)
    //       }
    //     })
    //   })
  }

  task.displayName = "mocha"
  return task
}

module.exports = mochaTask
