const ts = require("gulp-typescript")
const vfs = require("vinyl-fs")
const changed = require("gulp-changed")
const map = require("map-stream")
const sourcemaps = require("gulp-sourcemaps")
const mergeStream = require("merge-stream")

module.exports = ({ distPath }) => {
  const tsProject = ts.createProject("tsconfig.json")
  const typescript = (done) => {
    const tp = tsProject()
    try {
      const source = tsProject.src()
      const streamTsproject = source
        .pipe(changed(distPath, { extension: ".js" }))
        .pipe(sourcemaps.init())
        .pipe(tp)
        .on("error", done)

      const copyDTS = tsProject
        .src()
        .pipe(
          map((f, cb) => {
            if (f.path.endsWith("d.ts")) {
              cb(null, f)
            } else {
              cb(null)
            }
          })
        )
        .pipe(vfs.dest(distPath))
        .on("error", done)

      return mergeStream(
        streamTsproject.js.pipe(sourcemaps.write(".", { includeContent: false })).pipe(vfs.dest(distPath)),
        streamTsproject.dts.pipe(vfs.dest(distPath)),
        copyDTS
      ).on("end", done)
    } catch (err) {
      return done()
    }
  }

  return typescript
}
