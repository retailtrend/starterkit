const CoffeeScript = require("coffeescript")
const path = require("path")
const map = require("map-stream")
const vfs = require("vinyl-fs")
const changed = require("gulp-changed")

module.exports = ({ srcPattern, distPath }) => {
  const coffee = (done) => {
    return vfs
      .src(srcPattern)
      .pipe(changed(distPath, { extension: ".js" }))
      .pipe(
        map((file, cb) => {
          try {
            const ret = CoffeeScript.compile(file.contents.toString(), {
              filename: file.path,
              sourceMap: true,
              bare: true,
            })
            file.contents = Buffer.from(ret.js)
            const sourceMap = JSON.parse(ret.v3SourceMap)
            sourceMap.sourceRoot = file.base
            sourceMap.sources = [path.relative(file.base, file.path)]
            sourceMap.file = path.relative(file.base, file.path)
            sourceMap.sourcesContent = file.content
            file.sourceMap = sourceMap
            file.extname = ".js"
            cb(null, file)
          } catch (err) {
            err.filename = file.relative
            //            printError(err)
            cb(err)
          }
        })
      )
      .pipe(vfs.dest(distPath, { sourcemaps: "." }))
      .on("end", () => {
        if (done) done()
      })
  }

  return coffee
}
