import fs from "fs/promises"
import Mustache from "mustache"

export const generateMy = async (cfg) => {
  try {
    const files = await fs.readdir(cfg.path)

    for (const file of files) {
      if (["my.ts", "my.js", "my.coffee"].indexOf(file) >= 0) {
        return
      }
    }

    let output
    if (cfg.my?.template) {
      output = Mustache.render(cfg.my?.template, cfg.my?.variables)
    } else {
      output = Mustache.render((await fs.readFile(`${__dirname}/../../templates/my.ts.mst`)).toString())
    }

    await fs.writeFile(`${cfg.path}/my.ts`, output)
  } catch (err) {
    if (!(err.code == "ENOENT" && err.syscall == "scandir")) {
      console.warn("generateMy():", err)
    }
  }
}

export default generateMy
