require("@babel/register")
const packageJson = require("./package.json")
const { earlGreyTasks } = require("./src/index")
const gulp = require("gulp")

const tasks = earlGreyTasks(packageJson)
module.exports = {
  ...tasks,
  default: tasks.build,
  serve: gulp.series(
    tasks["babel"],
    tasks["coffee"],
    tasks["typescript"],
    gulp.parallel(tasks["babelWatch"], tasks["coffeeWatch"], tasks["coffeelintWatch"], tasks["eslintWatch"])
  ),
}
