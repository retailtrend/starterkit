"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateMy = exports.default = void 0;

var _promises = _interopRequireDefault(require("fs/promises"));

var _mustache = _interopRequireDefault(require("mustache"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const generateMy = async cfg => {
  try {
    const files = await _promises.default.readdir(cfg.path);

    for (const file of files) {
      if (["my.ts", "my.js", "my.coffee"].indexOf(file) >= 0) {
        return;
      }
    }

    let output;

    if (cfg.my?.template) {
      output = _mustache.default.render(cfg.my?.template, cfg.my?.variables);
    } else {
      output = _mustache.default.render((await _promises.default.readFile(`${__dirname}/../../templates/my.ts.mst`)).toString());
    }

    await _promises.default.writeFile(`${cfg.path}/my.ts`, output);
  } catch (err) {
    if (!(err.code == "ENOENT" && err.syscall == "scandir")) {
      console.warn("generateMy():", err);
    }
  }
};

exports.generateMy = generateMy;
var _default = generateMy;
exports.default = _default;
//# sourceMappingURL=generateMy.js.map