"use strict";

const gulpAwspublish = require("gulp-awspublish");

const vfs = require("vinyl-fs");

const map = require("map-stream");

const path = require("path");

const _ = require("lodash");

module.exports = ({
  bucket,
  srcPattern,
  distPath,
  options = {}
}) => {
  const awspublish = () => {
    const publisher = gulpAwspublish.create(_.merge(options, {
      params: {
        Bucket: bucket
      }
    }));
    return vfs.src(srcPattern).pipe(map((data, cb) => {
      data.path = path.join(distPath, path.relative(data.base, data.path));
      data.base = data.cwd;
      cb(null, data);
    })).pipe(publisher.publish()).pipe(publisher.cache()).pipe(gulpAwspublish.reporter());
  };

  return awspublish;
};
//# sourceMappingURL=awspublish.js.map