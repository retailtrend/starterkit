"use strict";

const moment = require("moment");

const fs = require("fs");

const execFile = require("child_process").execFile;

const path = require("path");

module.exports = ({
  distPath,
  properties = {}
}) => {
  const manifest = done => {
    execFile("git", ["rev-parse", "HEAD"], (err, commit) => {
      if (err) done(err);
      const manifestPath = path.join(distPath, "manifest.json");

      try {
        fs.accessSync(path.dirname(manifestPath));
      } catch (err) {
        fs.mkdirSync(path.dirname(manifestPath));
      }

      commit = commit.trim();
      fs.writeFile(path.join(distPath, "manifest.json"), Buffer.from(JSON.stringify({
        commit,
        createdAt: moment().utc().format(),
        ...properties
      })), done);
    });
  };

  return manifest;
};
//# sourceMappingURL=manifest.js.map