"use strict";

const apidoc = require("apidoc");

module.exports = ({
  srcPath,
  destPath
}) => {
  const apidocTask = done => {
    const doc = apidoc.createDoc({
      src: [srcPath],
      dest: destPath,
      excludeFilters: ["node_modules"]
    });

    if (typeof doc !== "boolean" || doc === true) {
      done();
    } else {
      done(new Error("apidoc"));
    }
  };

  apidocTask.displayName = "apidoc";
  return apidocTask;
};
//# sourceMappingURL=apidoc.js.map