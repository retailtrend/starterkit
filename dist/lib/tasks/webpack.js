"use strict";

const webpack = require("../../bin/webpack");

module.exports = ({
  config,
  options = {}
}) => {
  const task = done => {
    const execWebpack = config => {
      webpack(config, options, err => {
        if (err) done(err);else done();
      });
    };

    if (typeof config === "function") config = config();

    if (config.then) {
      config.then(config => execWebpack(config));
      return;
    }

    execWebpack(config);
  };

  task.displayName = "webpack";
  return task;
};
//# sourceMappingURL=webpack.js.map