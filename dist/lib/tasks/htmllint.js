"use strict";

const fs = require("fs");

const htmllint = require("htmllint");

const map = require("map-stream");

const vfs = require("vinyl-fs");

const cache = require("gulp-cached");

module.exports = htmlPath => {
  const _htmllint = done => {
    try {
      const opts = JSON.parse(fs.readFileSync("./.htmllintrc"));

      const printIssue = (filename, issues) => {
        issues.forEach(function (issue) {
          var msg = [`${filename}:${issue.line}:${issue.column}`, " ", htmllint.messages.renderIssue(issue)].join("");
          console.info(msg);
        });
      };

      htmllint.use(opts.plugins || []);
      delete opts.plugins;
      return vfs.src(htmlPath).pipe(cache("htmllint")).pipe(map((data, cb) => {
        htmllint(data.contents.toString(), opts).then(issues => {
          printIssue(data.relative, issues);
          cb(null, issues);
        }).catch(err => {
          console.error(err);
          cb(null);
        });
      })).on("end", () => {
        if (done) done();
      });
    } catch (err) {
      console.warn(".htmllintrc not exists.");
      if (done) done(null);
    }
  };

  _htmllint.displayName = "htmllint";
  return _htmllint;
};
//# sourceMappingURL=htmllint.js.map