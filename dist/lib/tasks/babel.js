"use strict";

const gulpBabel = require("gulp-babel");

const vfs = require("vinyl-fs");

const changed = require("gulp-changed");

const sourcemaps = require("gulp-sourcemaps");

const map = require("map-stream");

module.exports = ({
  srcPattern,
  distPath
}) => {
  const babel = done => vfs.src(srcPattern).pipe(changed(distPath, {
    extension: ".js"
  })).pipe(sourcemaps.init()).pipe(gulpBabel()).pipe(map((file, cb) => {
    file.sourceMap.sourceRoot = file.base;
    cb(null, file);
  })).pipe(vfs.dest(distPath, {
    sourcemaps: "."
  })).on("end", () => {
    if (done) done();
  });

  return babel;
};
//# sourceMappingURL=babel.js.map