"use strict";

const gulpTslint = require("gulp-tslint");

const vfs = require("vinyl-fs");

const cache = require("gulp-cached");

module.exports = ({
  srcPattern
}) => {
  const tslint = done => {
    return vfs.src(srcPattern).pipe(cache("tslint")).pipe(gulpTslint({
      tslint: require("tslint"),
      formatter: "verbose"
    })).pipe(gulpTslint.report());
  };

  return tslint;
};
//# sourceMappingURL=tslint.js.map