"use strict";

const sequelizeAuto = require("sequelize-auto");

const Promise = require("bluebird");

module.exports = ({
  db,
  user,
  pass,
  options
}) => {
  const task = done => {
    const auto = new sequelizeAuto(db, user, pass, options);
    const runPromisify = Promise.promisify(auto.run, {
      context: auto
    });
    runPromisify().catch(err => console.error(err)).finally(done);
  };

  task.displayName = "seqielizeAuto";
  return task;
};
//# sourceMappingURL=sequelize-auto.js.map