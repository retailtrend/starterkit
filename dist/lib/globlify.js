"use strict";

var path = require("path");

function pathGlobWithExt(paths, exts = []) {
  if (paths.constructor === Array) {
    paths = paths.map(p => p + "/**/*");
  } else {
    paths = [paths + "/**/*"];
  }

  return paths.map(p => exts.map(ext => path.normalize(p + `.${ext}`))).reduce((prev, next) => prev.concat(next));
}

module.exports = pathGlobWithExt;
//# sourceMappingURL=globlify.js.map