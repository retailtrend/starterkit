#!/usr/bin/env node
"use strict";

require("@babel/register");

var webpack = require("webpack");

var _ = require("lodash");

var path = require("path");

var webpackDevServer = require("webpack-dev-server");

var webpackCallback = function (err, stats) {
  if (err) {
    console.error("webpack:build", err);
  } else {
    let statOption = {
      assets: true,
      chunks: false,
      children: false,
      modules: true,
      colors: true
    };

    if (process.env.NODE_ENV == "development") {
      statOption = { ...statOption,
        assets: false,
        modules: false
      };
    }

    console.info("[webpack:build]", stats.toString(statOption));
  }
};

const standalone = (options = {}) => {
  const config = require(options.config).webpack;

  return run(config, options);
};

var run = (config = {
  entry: ["app"],
  vendor: [],
  polyfills: [],
  srcPath: path.join(process.cwd(), "./client"),
  distPath: path.join(process.cwd(), "/public"),
  publicPath: "/"
}, options = {}, cb) => {
  if (!config.devServer) config.devServer = {};

  var webpackConfig = require("../lib/webpack.config.js")(config);

  if (webpackConfig.entry.constructor != Object) {
    webpackConfig.entry = {};
  }

  options.host = options.host || "localhost";
  options.port = options.port || 8080;
  Object.keys(webpackConfig.entry).forEach(e => {
    let entry = [];
    if (config.polyfills) entry = entry.concat(config.polyfills);

    if (options.hot) {
      const hotLoader = [`webpack-dev-server/client?http://${options.host}:${options.port}`, "webpack/hot/only-dev-server"];
      entry.push(...hotLoader);
    }

    entry.push(e);
    webpackConfig.entry[e] = entry;
  });

  if (options.hot) {
    webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
    webpackConfig.output.publicPath = `http://${options.host}:${options.port}/`;
  }

  var compiler = webpack(webpackConfig);

  if (options.hot) {
    let serverOptions = {
      stats: {
        colors: true
      },
      historyApiFallback: true,
      // noInfo: true,
      contentBase: webpackConfig.output.path,
      hot: true,
      publicPath: webpackConfig.output.publicPath,
      headers: {
        "Access-Control-Allow-Origin": "*"
      }
    };
    if (options.host != "localhost" || options.host != "127.0.0.1") serverOptions.public = options.host;
    let server = new webpackDevServer(compiler, serverOptions);
    server.listen(options.port, "0.0.0.0", (err, result) => {
      if (err) console.error(err);else console.info(`webpack dev-server running for HMR on ${options.host}:${options.port}`);
    });
  } else if (options.watch) {
    console.info(`webpack dev-server running for watch`);
    compiler.watch({}, (err, stats) => {
      webpackCallback(err, stats);
      return cb && cb(err, stats);
    });
  } else {
    compiler.run((err, stats) => {
      webpackCallback(err, stats);

      if (!err && stats.hasErrors()) {
        const info = stats.toJson();
        err = new Error(info.errors.join("\n"));
      }

      return cb && cb(err, stats);
    });
  }

  return compiler;
};

module.exports = run;

if (require.main == module) {
  var opt = require("node-getopt").create([["c", "config=", "starterkit config file path"], ["w", "watch", "enable watchmode ignored when hot is enabled"], ["h", "hot", "enable hot replacement"]]).bindHelp().parseSystem();

  standalone(opt.options);
}
//# sourceMappingURL=webpack.js.map