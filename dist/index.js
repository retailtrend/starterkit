"use strict";

const gulp = require("gulp");

const path = require("path");

const bs = require("browser-sync").create();

const del = require("del");

const _ = require("lodash");

const {
  generateMy
} = require("./lib/generateMy");

const createApidoc = require("./lib/tasks/apidoc");

const createBabel = require("./lib/tasks/babel");

const createCoffeelint = require("./lib/tasks/coffeelint");

const createNodemon = require("./lib/tasks/nodemon");

const createManifest = require("./lib/tasks/manifest");

const createWebpack = require("./lib/tasks/webpack");

const createAwspublish = require("./lib/tasks/awspublish");

const createEslint = require("./lib/tasks/eslint"); //const createTslint = require("./lib/tasks/tslint")


const createHtmllint = require("./lib/tasks/htmllint");

const createCoffee = require("./lib/tasks/coffee");

const createMocha = require("./lib/tasks/mocha");

const createSequelizeAuto = require("./lib/tasks/sequelize-auto");

const createTypescript = require("./lib/tasks/typescript");

const waitPort = require("wait-port");

const Bluebird = require("bluebird");

const makeIgnoreError = task => {
  const f = done => {
    const t = task(err => {
      if (err) console.error(err);
      done();
    });
    return t;
  };

  if (task.displayName) f.displayName = task.displayName;else if (task.name) f.displayName = task.name;
  return f;
};
/**
 * Create gulp tasks for earlgrey project
 *
 * @param {string} config.frontendPath Root directory of frontend source
 * @param {string} config.backendPath Root directory of backend source
 * @param {string} config.testPath Root directory of test source
 * @param {string} config.testBuildPath Build output directory of test
 * @param {string} config.backendBuildPath Build output directory of backend
 * @param {string} config.frontendBuildPath Build output directory of frontend
 * @param {string} config.excludePattern exclude source pattern
 * @param {string} config.publicPath Build output directory of frontend(Deprecated. use frontendBuildPath)
 * @param {number} config.port Http server port
 * @param {string} config.apidocPath

 * @param {string} config.envConfig.path path of env config (eg: src/config/environment)
 * @param {string} config.envConfig.my.template Template of my.ts
 * @param {string} config.envConfig.my.variables Template variables

 * @param {array} config.webpack.entry
 * @param {array} config.webpack.polyfills polyfills
 * @param {object} config.webpack.babel module configurations for babel (see https://webpack.js.org/configuration/module)
 * @param {bool} config.webpack.mangle mangling or not
 * @param {object} config.webpack.sassLoader option of sass-loader (see https://github.com/webpack-contrib/sass-loader)
 * @param {array} config.webpack.htmlPlugins array of options of html-plugin (see https://github.com/jantimon/html-webpack-plugin)
 * @param {string} config.webpack.publicPath publicPath of webpack config
 *
 * @param {object} [config.sequelizeAuto=null] configurations for sequelizeAuto, see https://github.com/sequelize/sequelize-auto#programmatic-api
 *
 * @param {object} [config.apidocS3 Configurations=null] to upload apidoc to s3. It wouldn't upload NODE_ENV is not production or null.
 * @param {string} config.apidocS3.region AWS Region
 * @param {string} config.apidocS3.bucket S3 Bucket
 * @param {string} config.apidocS3.prefix S3 Bucket prefix
 *
 * @param {object} [config.cdnS3=null] Configurations to upload frontend to s3. It wouldn't upload NODE_ENV is not production or null.
 * @param {string} config.cdnS3.region AWS Region
 * @param {string} config.cdnS3.bucket S3 Bucket
 * @param {string} config.cdnS3.prefix S3 Bucket prefix
 *
 * @param {boolean} config.noManifest Don't generate manifest.json
 *
 * @param {object} options
 * @param {array} options.buildTasks Tasks which invoked before serve or build
 * @param {array} options.serveTasks Tasks which invoked with serve
 *
 */


const earlGreyTasks = (packageJson, config, options = {}) => {
  if (config && !config.frontendBuildPath && config.publicPath) {
    config.frontendBuildPath = config.publidPath;
  }

  config = _.merge({
    excludePattern: ["**/common-library/**/*"],
    frontendPath: "src/client",
    backendPath: "src",
    testPath: "test",
    backendBuildPath: "dist",
    frontendBuildPath: "dist/public",
    publicPath: "dist/public",
    testBuildPath: "testDist",
    apidocPath: "apidoc",
    webpack: null,
    apidocS3: null,
    cdnS3: null,
    awsCredentials: {
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      region: process.env.AWS_DEFAULT_REGION
    },
    envConfig: {
      path: "src/config/environment"
    }
  }, config);

  if (config.webpack) {
    config.webpack = _.merge({
      entry: ["app"],
      polyfills: [],
      mangle: true,
      sassLoader: {},
      htmlPlugins: []
    }, config.webpack);
  }

  if (config.apidocS3) {
    if (!config.apidocS3.bucket || !config.apidocS3.prefix) throw Error("Invalid apidoc upload config");
  }

  if (config.cdnS3) {
    if (!config.cdnS3.bucket || !config.cdnS3.prefix) throw Error("Invalid cloudfront upload config");
  }

  const hiddenPattern = [`!${path.join("**", ".*")}`];
  const excludePattern = config.excludePattern.map(p => `!${p}`).concat(hiddenPattern);
  const backendJsPattern = path.join(config.backendPath, "**/*.js?(x)");
  const backendTsPattern = path.join(config.backendPath, "**/*.ts?(x)");
  const backendCoffeePattern = path.join(config.backendPath, "**/*.coffee");
  const frontendJsPattern = path.join(config.frontendPath, "**/*.js?(x)");
  const frontendTsPattern = path.join(config.frontendPath, "**/*.ts?(x)");
  const frontendCoffeePattern = path.join(config.frontendPath, "**/*.coffee");
  const htmlPattern = [path.join(config.frontendPath, "**/*.html"), path.join(config.backendPath, "**/*.html")];
  const excludeNodeModules = "!**/node_modules/**";
  let Tasks = [];

  const emptyTask = done => done();

  const coffeelint = createCoffeelint({
    srcPattern: [backendCoffeePattern, frontendCoffeePattern].concat(hiddenPattern)
  });
  const eslint = createEslint({
    srcPattern: ["?(gulpfile.babel.js)", frontendJsPattern, frontendTsPattern, backendJsPattern, backendTsPattern, excludeNodeModules].concat(excludePattern)
  }); // const tslint = createTslint({
  //   srcPattern: ["?(gulpfile.babel.ts)", frontendTsPattern, backendTsPattern].concat(hiddenPattern)
  // })

  const htmllint = createHtmllint(htmlPattern.concat(excludePattern));
  const babel = createBabel({
    srcPattern: [backendJsPattern, `!${frontendJsPattern}`].concat(excludePattern),
    distPath: config.backendBuildPath
  });
  const typescript = createTypescript({
    srcPattern: [backendTsPattern, `!${frontendTsPattern}`].concat(excludePattern),
    distPath: config.backendBuildPath
  });
  const coffee = createCoffee({
    srcPattern: [backendCoffeePattern, `!${frontendCoffeePattern}`].concat(excludePattern),
    distPath: config.backendBuildPath
  });
  const nodemonOpts = {
    watchPath: config.backendBuildPath,
    exec: packageJson.main,
    options: {
      appPort: config.port
    },
    callbacks: {
      start: () => {
        if (config.port && config.webpack) {
          let reloadEventFired = false;
          let bsPort = config.bsPort || 3000;
          bs.init({
            port: bsPort,
            ui: {
              port: bsPort
            },
            files: [{
              match: path.join(config.frontendBuildPath, "**", "*.{js,css,html}"),
              fn: async function () {
                if (reloadEventFired) {
                  return;
                }

                reloadEventFired = true;
                await Bluebird.delay(500);
                const opened = await waitPort({
                  host: "localhost",
                  port: config.port
                });
                reloadEventFired = false;

                if (opened) {
                  bs.reload();
                }
              }
            }],
            proxy: {
              target: `http://localhost:${config.port}`,
              ws: true,
              proxyReq: [(proxyReq, req, res) => {
                proxyReq.setHeader("Host", req.headers.host);

                if (req.socket.remoteAddress) {
                  proxyReq.setHeader("X-Forwarded-For", req.socket.remoteAddress);
                } else {
                  console.error(req.socket);
                }
              }]
            }
          });
        }
      }
    }
  };

  if (config.frontendBuildPath) {
    nodemonOpts.options.watchIgnorePatterns = [path.join(config.frontendBuildPath, "**/*")];
  }

  const nodemon = createNodemon(nodemonOpts);
  const manifest = createManifest({
    distPath: config.backendBuildPath,
    properties: {
      version: packageJson.version
    }
  });
  const apidoc = createApidoc({
    srcPath: config.backendPath,
    destPath: config.apidocPath
  });
  const mocha = createMocha({
    srcPattern: path.join(config.testBuildPath, "index.js")
  });

  const clean = () => {
    return del([config.frontendBuildPath, config.backendBuildPath, config.testBuildPath, config.apidocPath]);
  };

  const coffeelintWatch = () => gulp.watch([frontendCoffeePattern, backendCoffeePattern].concat(excludePattern)).on("change", f => createCoffeelint({
    srcPattern: f
  })());

  const eslintWatch = () => gulp.watch([frontendJsPattern, frontendTsPattern, backendTsPattern, backendJsPattern].concat(excludePattern)).on("change", f => createEslint({
    srcPattern: f
  })());

  const babelWatch = () => gulp.watch([backendJsPattern, `!${frontendJsPattern}`].concat(excludePattern), gulp.series(makeIgnoreError(babel)));

  const typescriptWatch = () => gulp.watch([backendTsPattern, `!${frontendTsPattern}`].concat(excludePattern), gulp.series(makeIgnoreError(typescript)));

  const coffeeWatch = () => gulp.watch([backendCoffeePattern, `!${frontendCoffeePattern}`].concat(excludePattern), gulp.series(makeIgnoreError(coffee)));

  const htmllintWatch = () => gulp.watch(htmlPattern.concat(excludePattern)).on("change", f => createHtmllint(f)());

  const apidocWatch = () => gulp.watch([config.backendPath].concat(excludePattern), gulp.series(makeIgnoreError(apidoc)));

  Tasks = [coffeelint, eslint, htmllint, babel, typescript, coffee, apidoc, manifest, mocha, nodemon, clean, coffeelintWatch, eslintWatch, babelWatch, typescriptWatch, coffeeWatch, htmllintWatch];
  let apidocS3 = emptyTask;

  if (config.apidocS3) {
    apidocS3 = createAwspublish({
      bucket: config.apidocS3.bucket,
      srcPattern: path.join(config.apidocPath, "**/*"),
      distPath: path.join(config.apidocS3.prefix, packageJson.version),
      options: { ...config.awsCredentials,
        region: config.apidocS3.region || config.awsCredentials.region || "us-west-1"
      }
    });
    apidocS3.displayName = "apidocS3";
    Tasks.push(apidocS3);
  }

  let cdnS3 = emptyTask;

  if (config.cdnS3) {
    cdnS3 = createAwspublish({
      bucket: config.cdnS3.bucket,
      srcPattern: [path.join(config.frontendBuildPath, "**/*"), "!" + path.join(config.frontendBuildPath, "**/*.gz")],
      distPath: `${config.cdnS3.prefix}/${packageJson.version}`,
      options: { ...config.awsCredentials,
        region: config.cdnS3.region || config.awsCredentials.region || "us-west-1"
      }
    });
    cdnS3.displayName = "cdnS3";
    Tasks.push(cdnS3);
  }

  let sequelizeAuto = emptyTask;

  if (config.sequelizeAuto) {
    sequelizeAuto = createSequelizeAuto(config.sequelizeAuto);
  }

  let webpack = emptyTask;
  let webpackWatch = emptyTask;

  if (config.webpack) {
    const webpackConfig = {
      srcPath: path.isAbsolute(config.frontendPath) ? config.frontendPath : path.join(process.cwd(), config.frontendPath),
      distPath: path.isAbsolute(config.frontendBuildPath) ? config.frontendBuildPath : path.join(process.cwd(), config.frontendBuildPath),
      publicPath: "/",
      ...config.webpack
    };
    webpack = createWebpack({
      config: webpackConfig
    });
    webpackWatch = createWebpack({
      config: webpackConfig,
      options: {
        watch: true
      }
    });
    webpackWatch.displayName = "webpackWatch";
    Tasks.push(webpack);
    Tasks.push(webpackWatch);
  }

  const myEnv = async () => generateMy(config.envConfig ?? {});

  const createBuild = () => {
    let paTasks = [];

    if (config.noManifest != true) {
      paTasks.push(manifest);
    }

    paTasks = paTasks.concat([coffee, babel, typescript, apidoc]);
    const buildTasks = options.buildTasks || [];
    if (config.webpack) paTasks.push(webpack);
    const tasks = [clean, myEnv, done => gulp.parallel.apply(null, paTasks)(done)].concat(buildTasks);
    const f = gulp.series.apply(null, tasks);
    f.displayName = "build";
    return f;
  };

  const build = createBuild();

  const createServe = () => {
    let paBuildTasks = [];

    if (config.noManifest != true) {
      paBuildTasks.push(manifest);
    }

    paBuildTasks = paBuildTasks.concat([makeIgnoreError(coffee), makeIgnoreError(babel), makeIgnoreError(typescript), makeIgnoreError(apidoc)]);
    const buildTasks = options.buildTasks || [];
    const paServeTasks = [typescriptWatch, babelWatch, coffeeWatch, htmllintWatch, coffeelintWatch, eslintWatch, apidocWatch, nodemon];
    const serveTasks = options.serveTasks || [];
    if (config.webpack) paBuildTasks.push(webpackWatch);
    if (config.sequelizeAuto) paBuildTasks.push(sequelizeAuto);
    const tasks = [clean].concat([myEnv]).concat([gulp.parallel(...paBuildTasks)]).concat(buildTasks).concat([gulp.parallel(...paServeTasks, ...serveTasks)]);
    const f = gulp.series.apply(null, tasks);
    f.displayName = "serve";
    return f;
  };

  const serve = createServe();
  const testCoffeePattern = path.join(config.testPath, "**/*.coffee");
  const testJsPattern = path.join(config.testPath, "**/*.js?(x)");
  const testTsPattern = path.join(config.testPath, "**/*.ts?(x)");
  const testCoffee = createCoffee({
    srcPattern: [testCoffeePattern],
    distPath: config.testBuildPath
  });
  testCoffee.displayName = "testCoffee";
  const testBabel = createBabel({
    srcPattern: [testJsPattern],
    distPath: config.testBuildPath
  });
  testBabel.displayName = "testBabel";
  const testTypescript = createTypescript({
    srcPattern: [testTsPattern],
    distPath: config.testBuildPath
  });
  testTypescript.displayName = "testTypescript";

  const testBabelWatch = () => gulp.watch(testJsPattern, gulp.series(testBabel)).on("error", err => {});

  const testTypescriptWatch = () => gulp.watch(testTsPattern, gulp.series(testTypescript)).on("error", err => {});

  const testCoffeeWatch = () => gulp.watch(testCoffeePattern, gulp.series(testCoffee)).on("error", err => {});

  const test = gulp.series(build, testCoffee, testBabel, testTypescript, mocha);
  test.displayName = "test";

  const testWatch = () => gulp.watch([path.join(config.buildPath, "**", "*.js"), path.join(config.testBuildPath, "**", "*.js")]).on("change", f => {
    return createMocha({
      srcPattern: f
    })();
  });

  testWatch.displayName = "testWatch";

  const createTestServe = () => {
    const testIgnoreFailure = gulp.series(manifest, makeIgnoreError(coffee), makeIgnoreError(babel), makeIgnoreError(typescript), makeIgnoreError(testCoffee), makeIgnoreError(testBabel), makeIgnoreError(testTypescript));
    return gulp.series(done => {
      process.env.NODE_ENV = "test";
      done();
    }, testIgnoreFailure, gulp.parallel(babelWatch, typescriptWatch, coffeeWatch, testBabelWatch, testTypescriptWatch, testCoffeeWatch, testWatch));
  };

  const testServe = createTestServe();
  testServe.displayName = "testServe";
  const lint = gulp.series(myEnv, eslint, //tslint,
  coffeelint, htmllint, apidoc);
  lint.displayName = "lint";
  return Tasks.concat([myEnv, build, lint, serve, test, testServe, testCoffee, testBabel]).reduce((tasks, f) => {
    tasks[f.displayName || f.name] = f;
    return tasks;
  }, {});
};

module.exports = {
  earlGreyTasks,
  generateMy
};
//# sourceMappingURL=index.js.map
