export interface IEarlgreyTaskConfig {
  excludePattern: string[]
  frontendPath?: string
  backendPath?: string
  buildPath?: string
  testPath?: string
  testBuildPath?: string
  backendBuildPath?: string
  frontendBuildPath?: string
  port?: number
  apidocPath: string
  webpack?: {
    entry: string[]
    polyfills: string[]
    babel: object
    mangle: boolean
    htmlPlugins: object[]
    publicPath: string
  }
  sequalizeAuto?: object
  apidocS3?: {
    region: string
    bucket: string
    prefix: string
  }
  cdnS3?: {
    region: string
    bucket: string
    prefix: string
  }
}

export interface IEarlgreyTaskOption {
  buildTasks?: any[]
  serveTasks?: any[]
}

export declare function earlGreyTasks(packageJson: object, config?: IEarlgreyTaskConfig, options?: IEarlgreyTaskOption): any
