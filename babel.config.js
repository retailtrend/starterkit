module.exports = {
  babelrcRoots: true,
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          node: true,
        },
      },
    ],
  ],
}
