module.exports = {
  root: true,
  env: {
    es6: true,
    browser: true,
    commonjs: true,
    node: true,
    mocha: true
  },
  extends: ["eslint:recommended", "plugin:prettier/recommended"],
  parser: "@babel/eslint-parser",
  plugins: ["mocha", "prettier"],
  rules: {
    indent: "off",
    "no-extra-semi": "off",
    "no-control-regex": 0,
    "no-unreachable": ["warn"],
    "no-fallthrough": ["warn"],
    "no-empty": ["error", { allowEmptyCatch: true }],
    "no-constant-condition": ["warn"],
    "no-unused-vars": [
      "warn",
      {
        args: "none"
      }
    ],
    "no-console": ["warn", { allow: ["info", "warn", "error"] }]
  },
  globals: {
    $: true,
    Promise: true,
    define: true,
    jQuery: true,
    moment: true
  }
}
